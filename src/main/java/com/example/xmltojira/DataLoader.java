package com.example.xmltojira;

import com.example.xmltojira.model.Issue;
import com.example.xmltojira.model.IssuePriority;
import com.example.xmltojira.model.IssueTypeCodesMap;
import com.example.xmltojira.model.ProjectData;
import com.example.xmltojira.services.ProjectDataUnmarshallingService;
import com.example.xmltojira.services.jira.JiraClientService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class DataLoader implements CommandLineRunner {

    private final ProjectDataUnmarshallingService projectDataUnmarshallingService;
    private final JiraClientService jiraClientService;
    private final IssueTypeCodesMap issueTypeCodesMap;

    public DataLoader(ProjectDataUnmarshallingService projectDataUnmarshallingService,
                      JiraClientService jiraClientService,
                      IssueTypeCodesMap issueTypeCodesMap) {

        this.projectDataUnmarshallingService = projectDataUnmarshallingService;
        this.jiraClientService = jiraClientService;
        this.issueTypeCodesMap = issueTypeCodesMap;
    }

    @Override
    public void run(String... args) throws Exception {
        Resource resource = new ClassPathResource("Data/InputFile.xml");
        File inputFile = resource.getFile();

        ProjectData projectData = projectDataUnmarshallingService.getProjectData(inputFile);

        for (Issue issue : projectData.getIssues().getIssues()) {
            String id = jiraClientService.createIssue(
                    projectData.getProjectKey(),
                    issueTypeCodesMap.getIssueTypeId(issue),
                    "Issue from xml",
                    projectData.getIssueDescriptionSummary(issue),
                    IssuePriority.parsePriority(issue.getPriority())
            );

            System.out.println("Created new Issue with ID = " + id);
        }

    }
}
