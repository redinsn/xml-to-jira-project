package com.example.xmltojira.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:jira.properties")
public class PropertiesConfig {

}
