package com.example.xmltojira;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XmlToJiraApplication {

	public static void main(String[] args) {
		SpringApplication.run(XmlToJiraApplication.class, args);
	}

}
