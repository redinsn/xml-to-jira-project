package com.example.xmltojira.model;

public enum IssuePriority {

    CRITICAL("Critical priority"),
    HIGHEST("This problem will block progress"),
    HIGH("Serious problem that could block progress"),
    MEDIUM("Has the potential to affect progress"),
    LOW("Minor problem or easily worked around"),
    LOWEST("Trivial problem with little or no impact on progress");

    private String description;

    IssuePriority(String description) {
        this.description = description;
    }

    public static IssuePriority parsePriority(String priority) {

        switch (priority.toLowerCase()) {
            case "critical":
                return CRITICAL;
            case "highest":
                return HIGHEST;
            case "high":
                return HIGH;
            case "medium":
                return MEDIUM;
            case "low":
                return LOW;
            case "lowest":
                return LOWEST;
            default:
                return MEDIUM;
        }
    }
}
