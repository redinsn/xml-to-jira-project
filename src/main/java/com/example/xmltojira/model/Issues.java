package com.example.xmltojira.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;

@XmlRootElement(name = "issues")
public class Issues {

    private Set<Issue> issues;

    public Issues() {
    }

    @XmlElement(name = "issue")
    public Set<Issue> getIssues() {
        return issues;
    }

    public void setIssues(Set<Issue> issues) {
        this.issues = issues;
    }
}
