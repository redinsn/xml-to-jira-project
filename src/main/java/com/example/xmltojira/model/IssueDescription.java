package com.example.xmltojira.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "description")
public class IssueDescription {

    private Long id;
    private String summary;

    public IssueDescription() {
    }

    public IssueDescription(Long id, String summary) {
        this.id = id;
        this.summary = summary;
    }

    @XmlElement(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlElement(name = "summary")
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
