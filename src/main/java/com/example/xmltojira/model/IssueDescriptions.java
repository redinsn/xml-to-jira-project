package com.example.xmltojira.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;

@XmlRootElement(name = "descriptions")
public class IssueDescriptions {

    private Set<IssueDescription> issueDescriptions;

    public IssueDescriptions() {
    }

    @XmlElement(name = "description")
    public Set<IssueDescription> getIssueDescriptions() {
        return issueDescriptions;
    }

    public void setIssueDescriptions(Set<IssueDescription> issueDescriptions) {
        this.issueDescriptions = issueDescriptions;
    }
}
