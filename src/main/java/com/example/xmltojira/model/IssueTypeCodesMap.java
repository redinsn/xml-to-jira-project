package com.example.xmltojira.model;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class IssueTypeCodesMap {

    private Map<String, Long> typeToCodeMap;

    public IssueTypeCodesMap() {
        typeToCodeMap = new HashMap<>();
        typeToCodeMap.put("story", 10014L);
        typeToCodeMap.put("bug", 10017L);
        typeToCodeMap.put("epic", 10000L);
        typeToCodeMap.put("task", 10009L);
    }

    public Long getIssueTypeId(Issue issue) {
        return typeToCodeMap.get(issue.getType());
    }
}
