package com.example.xmltojira.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "data")
public class ProjectData {

    private String projectKey;
    private Issues issues;
    private IssueDescriptions issueDescriptions;

    public ProjectData() {
    }

    public ProjectData(String projectKey, Issues issues, IssueDescriptions issueDescriptions) {
        this.projectKey = projectKey;
        this.issues = issues;
        this.issueDescriptions = issueDescriptions;
    }

    @XmlElement(name = "projectKey")
    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    @XmlElement(name = "issues")
    public Issues getIssues() {
        return issues;
    }

    public void setIssues(Issues issues) {
        this.issues = issues;
    }

    @XmlElement(name = "descriptions")
    public IssueDescriptions getIssueDescriptions() {
        return issueDescriptions;
    }

    public void setIssueDescriptions(IssueDescriptions issueDescriptions) {
        this.issueDescriptions = issueDescriptions;
    }

    public String getIssueDescriptionSummary(Issue issue) {
        String result = null;
        for (IssueDescription issueDescription : this.issueDescriptions.getIssueDescriptions()) {
            if (issue.getId().equals(issueDescription.getId())) result = issueDescription.getSummary();
        }
        return result;
    }
}
