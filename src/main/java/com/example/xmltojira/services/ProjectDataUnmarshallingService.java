package com.example.xmltojira.services;

import com.example.xmltojira.model.ProjectData;

import java.io.File;

public interface ProjectDataUnmarshallingService {

    ProjectData getProjectData(File xmlFile);
}
