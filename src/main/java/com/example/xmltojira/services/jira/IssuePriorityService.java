package com.example.xmltojira.services.jira;

import com.atlassian.jira.rest.client.api.domain.BasicPriority;
import com.example.xmltojira.model.IssuePriority;

public interface IssuePriorityService {
    BasicPriority getPriority(IssuePriority priority);
}
