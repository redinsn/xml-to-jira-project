package com.example.xmltojira.services.jira;

import com.atlassian.jira.rest.client.api.AuthenticationHandler;
import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.auth.BasicHttpAuthenticationHandler;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.example.xmltojira.model.IssuePriority;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
public class JiraClientWithBasicAuth implements JiraClientService {

    @Value("${jira.username}")
    private String userName;

    @Value("${jira.password}")
    private String password;

    @Value("${jira.url}")
    private String jiraUrl;

    private IssuePriorityService issuePriorityService;

    public JiraClientWithBasicAuth(IssuePriorityService issuePriorityService) {
        this.issuePriorityService = issuePriorityService;
    }

    private JiraRestClient getJiraRestClient() {
        AsynchronousJiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
        AuthenticationHandler authenticationHandler = new BasicHttpAuthenticationHandler(this.userName, this.password);

        return factory.create(getJiraUri(), authenticationHandler);
    }

    private URI getJiraUri() {
        return URI.create(this.jiraUrl);
    }

    @Override
    public String createIssue(String projectKey, Long issueType, String summary, String description, IssuePriority priority) {
        IssueRestClient issueClient = getJiraRestClient().getIssueClient();

        IssueInput newIssue = new IssueInputBuilder(projectKey, issueType, summary)
                .setDescription(description)
                .setPriority(issuePriorityService.getPriority(priority))
                .build();

        return issueClient.createIssue(newIssue).claim().getKey();
    }

}
