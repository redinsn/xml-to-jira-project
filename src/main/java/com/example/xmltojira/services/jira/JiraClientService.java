package com.example.xmltojira.services.jira;

import com.atlassian.jira.rest.client.api.domain.Issue;
import com.example.xmltojira.model.IssuePriority;

public interface JiraClientService {

    String createIssue(String projectKey, Long issueType, String summary, String description, IssuePriority priority);
}
