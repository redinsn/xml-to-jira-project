package com.example.xmltojira.services.jira;

import com.atlassian.jira.rest.client.api.domain.BasicPriority;
import com.example.xmltojira.model.IssuePriority;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Component
public class IssuePriorityServiceImpl implements IssuePriorityService {

    private Map<IssuePriority, BasicPriority> priorityMap;

    @Value("${jira.url}")
    private String jiraUrl;

    public IssuePriorityServiceImpl() {
    }

    @PostConstruct
    private void init() {
        priorityMap = new HashMap<>();

        URI jiraUri = URI.create(jiraUrl);
        priorityMap.put(IssuePriority.CRITICAL, new BasicPriority(jiraUri, 1L, "Critical"));
        priorityMap.put(IssuePriority.HIGHEST, new BasicPriority(jiraUri, 1L, "Highest"));
        priorityMap.put(IssuePriority.HIGH, new BasicPriority(jiraUri, 2L, "High"));
        priorityMap.put(IssuePriority.MEDIUM, new BasicPriority(jiraUri, 3L, "Medium"));
        priorityMap.put(IssuePriority.LOW, new BasicPriority(jiraUri, 4L, "Low"));
        priorityMap.put(IssuePriority.LOWEST, new BasicPriority(jiraUri, 5L, "Lowest"));

    }

    @Override
    public BasicPriority getPriority(IssuePriority priority) {
        return priorityMap.get(priority);
    }
}
