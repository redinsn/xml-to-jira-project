package com.example.xmltojira.services;

import com.example.xmltojira.model.ProjectData;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

@Service
public class ProjectDataUnmarshallingServiceImpl implements ProjectDataUnmarshallingService {
    @Override
    public ProjectData getProjectData(File xmlFile) {

        ProjectData projectData = null;

        try {
            JAXBContext context = JAXBContext.newInstance(ProjectData.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            projectData = (ProjectData) unmarshaller.unmarshal(xmlFile);

        } catch (JAXBException ex) {
            ex.printStackTrace();
        }

        return projectData;
    }
}
